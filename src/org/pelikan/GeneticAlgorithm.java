package org.pelikan;

import org.apache.commons.cli.*;
import org.pelikan.object.Population;
import org.pelikan.object.PopulationsPool;
import org.pelikan.util.FitnessFunction;

import java.io.PrintWriter;

public class GeneticAlgorithm {
    /////////////////////
    //    CONSTANTS    //
    /////////////////////
    private static final String ATTRIBUTE_FACTOR_A  = "a",
                                ATTRIBUTE_FACTOR_B  = "c",
                                ATTRIBUTE_FACTOR_C  = "b",

                                ATTRIBUTE_POPULATION_SIZE   = "s",
                                ATTRIBUTE_POPULATIONS_COUNT = "p",
                                ATTRIBUTE_MUTATION_CHANCE   = "m",
                                ATTRIBUTE_CROSSOVER_CHANCE  = "x";


    /////////////////////
    //    VARIABLES    //
    /////////////////////
    private static double factorA, factorB, factorC;

    private static int populationSize, populationsCount;

    private static float mutationChance, crossoverChance;


    /////////////////////
    //    FUNCTIONS    //
    /////////////////////
    public static void main(String[] args) {
        CommandLine cmd = parseArguments(args);

        prepareVariables(cmd);

        System.out.println(factorA);
        System.out.println(factorB);
        System.out.println(factorC);

        FitnessFunction fitnessFunction = actor -> {
            int x = actor.getGeneValue();
            return (factorA * Math.pow(x, 2)) + (factorB * x) + factorC;
        };

        PopulationsPool populationsPool = new PopulationsPool();

        for (int i = 0; i < populationsCount; i++) {
            populationsPool.addPopulation(
                    new Population(populationSize, fitnessFunction, mutationChance, crossoverChance)
            );
        }

        System.out.println(populationsPool);

    }

    private static void prepareVariables(CommandLine cmd) {
        factorA = getFloatAttribute(cmd, ATTRIBUTE_FACTOR_A);
        factorB = getFloatAttribute(cmd, ATTRIBUTE_FACTOR_B);
        factorC = getFloatAttribute(cmd, ATTRIBUTE_FACTOR_B);

        populationSize   = getIntegerAttribute(cmd, ATTRIBUTE_POPULATION_SIZE);
        populationsCount = getIntegerAttribute(cmd, ATTRIBUTE_POPULATIONS_COUNT);

        mutationChance  = getFloatAttribute(cmd, ATTRIBUTE_MUTATION_CHANCE);
        crossoverChance = getFloatAttribute(cmd, ATTRIBUTE_CROSSOVER_CHANCE);
    }

    private static String getErrMsg(String attributeName) {
        return "Couldn't parse the attribute '" + attributeName + "' ";
    }

    private static float getFloatAttribute(CommandLine cmd, String attrName) {
        try {
            return Float.parseFloat(cmd.getOptionValue(attrName));
        } catch (Exception e) {
            printHelpAndExit(e, getErrMsg(attrName));
            return -1f; // <- will never be executed
        }
    }

    private static int getIntegerAttribute(CommandLine cmd, String attrName) {
        try {
            return Integer.parseInt(cmd.getOptionValue(attrName));
        } catch (Exception e) {
            printHelpAndExit(e, getErrMsg(attrName));
            return -1; // <- will never be executed
        }
    }

    private static CommandLine parseArguments(String[] args) {
        CommandLineParser parser          = new DefaultParser();
        Options           preparedOptions = getPreparedOptions();
        CommandLine       result          = null;


        try {
            result = parser.parse(preparedOptions, args);
        } catch (ParseException e) {
           printHelpAndExit(e);
        }


        return result;
    }

    private static void printHelpAndExit(Exception e) {
        printHelpAndExit(e, "");
    }

    private static void printHelpAndExit(Exception e, String msg) {
        PrintWriter printWriter = new PrintWriter(System.err);
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp(printWriter, 100,"GeneticAlgorithm", msg + e.getMessage(), getPreparedOptions(), 4, 10, "Try again ¯\\_(ツ)_/¯");

        printWriter.flush();

        System.exit(-1);
    }

    private static Options getPreparedOptions() {
        Options options = new Options();

        // Factors
        Option factorA = new Option(ATTRIBUTE_FACTOR_A, "factorA", true, "Factor a");
        factorA.setRequired(true);
        options.addOption(factorA);

        Option factorB = new Option(ATTRIBUTE_FACTOR_B, "factorB", true, "Factor b");
        factorB.setRequired(true);
        options.addOption(factorB);

        Option factorC = new Option(ATTRIBUTE_FACTOR_C, "factorC", true, "Factor c");
        factorC.setRequired(true);
        options.addOption(factorC);


        // Population size
        Option populationSize = new Option(ATTRIBUTE_POPULATION_SIZE, "populationSize", true, "The size of a population");
        populationSize.setRequired(true);
        options.addOption(populationSize);


        // Populations count
        Option populationCount = new Option(ATTRIBUTE_POPULATIONS_COUNT, "populationsCount", true, "The number of populations");
        populationCount.setRequired(true);
        options.addOption(populationCount);


        // Mutation probability
        Option mutationChance = new Option(ATTRIBUTE_MUTATION_CHANCE, "mutationChance", true, "The probability of a mutation");
        mutationChance.setRequired(true);
        options.addOption(mutationChance);

        // Mutation probability
        Option crossoverChance = new Option(ATTRIBUTE_CROSSOVER_CHANCE, "crossoverChance", true, "The probability of a crossover");
        crossoverChance.setRequired(true);
        options.addOption(crossoverChance);



        return options;
    }
}
