package org.pelikan.util;

public class IllegalMaskSizeException extends IllegalArgumentException {
    public IllegalMaskSizeException() {
        this("Mask must be bigger than or equal to 1 and smaller than 8!");
    }

    public IllegalMaskSizeException(String s) {
        super(s);
    }

    public IllegalMaskSizeException(String message, Throwable cause) {
        super(message, cause);
    }

    public IllegalMaskSizeException(Throwable cause) {
        super(cause);
    }
}
