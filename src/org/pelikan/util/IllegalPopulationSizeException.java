package org.pelikan.util;

@SuppressWarnings({"unused", "WeakerAccess"})
public class IllegalPopulationSizeException extends IllegalArgumentException {
    public IllegalPopulationSizeException() {
        this("Population size must be a multiple of 2!");
    }

    public IllegalPopulationSizeException(String s) {
        super(s);
    }

    public IllegalPopulationSizeException(String message, Throwable cause) {
        super(message, cause);
    }

    public IllegalPopulationSizeException(Throwable cause) {
        super(cause);
    }
}
