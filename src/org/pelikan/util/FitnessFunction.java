package org.pelikan.util;

import org.pelikan.object.Actor;

@FunctionalInterface
@SuppressWarnings("unused")
public interface FitnessFunction {
    double evaluate(Actor actor);
}
