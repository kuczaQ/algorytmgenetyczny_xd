package org.pelikan.object;

import org.pelikan.util.FitnessFunction;
import org.pelikan.util.IllegalMaskSizeException;
import org.pelikan.util.IllegalPopulationSizeException;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@SuppressWarnings("WeakerAccess")
public class Population {
    private List<Actor> actors;
    private float crossoverChance;


    public Population(int populationSize, FitnessFunction fitnessFunction, float mutationChance, float crossoverChance) {
        this.actors = new ArrayList<>();
        this.crossoverChance = crossoverChance;

        if (populationSize % 2 != 0)
            throw new IllegalPopulationSizeException();

        for (int i = 0; i < populationSize; i++) {
            actors.add(new Actor(mutationChance, fitnessFunction));
        }
    }

    public void doCrossover() {
        List<Actor> pool = new ArrayList<>(actors);

        do {
            Actor parent1 = pool.remove(getRandomIndex(pool.size()));
            Actor parent2 = pool.remove(getRandomIndex(pool.size()));

            if (crossover()) {
                int slicingPoint = getRandomIndex(8);

                byte byteMask = getByteMask(slicingPoint);
                byte byteMaskInverted = (byte) ~byteMask;


                byte parent1Gene = parent1.getRawGene();
                byte parent2Gene = parent2.getRawGene();

                byte child1Gene = (byte) ((parent1Gene & byteMask) | (parent2Gene & byteMaskInverted));
                byte child2Gene = (byte) ((parent1Gene & byteMaskInverted) | (parent2Gene & byteMask));

                Actor child1 = new Actor(child1Gene, parent1.getFitness(), parent1.getMutationChance());
                Actor child2 = new Actor(child2Gene, parent1.getFitness(), parent1.getMutationChance());

                actors.add(child1);
                actors.add(child2);
            }
        } while (pool.size() != 0);
    }

    public void doMutate() {
        actors.forEach(Actor::doMutate);
    }

    private byte getByteMask(int toMask) {
        switch (toMask) {
            case 1: return (byte) 0x80;
            case 2: return (byte) 0xC0;
            case 3: return (byte) 0xE0;
            case 4: return (byte) 0xF0;
            case 5: return (byte) 0xF8;
            case 6: return (byte) 0xFC;
            case 7: return (byte) 0xFE;

            default: throw new IllegalMaskSizeException();
        }
    }

    private int getRandomIndex(int limit) {
        return (new Random().nextInt(limit) + 1) % limit;
    }

    private boolean crossover() {
        return Math.random() <= crossoverChance;
    }

    @Override
    public String toString() {
        StringBuilder res = new StringBuilder("Population{" +
                "actors=");

        for (Actor actor : actors) {
            res
                .append("\n\t")
                .append(actor);
        }

        res.append("\n}");

        return res.toString();
    }
}
