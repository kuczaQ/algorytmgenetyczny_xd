package org.pelikan.object;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
public class PopulationsPool {
    private List<Population> populations;

    public PopulationsPool() {
        this.populations = new ArrayList<>();
    }

    public int getPopulationsCount() {
        return populations.size();
    }

    public void addPopulation(Population population) {
        populations.add(population);
    }

    @Override
    public String toString() {
        StringBuilder res = new StringBuilder("PopulationsPool{" +
                "populations=");

        for (Population population : populations) {
            res
                .append("\n\t")
                .append(population);
        }

        res.append("\n}");

        return res.toString();
    }
}
