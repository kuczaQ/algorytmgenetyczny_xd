package org.pelikan.object;

import org.pelikan.util.FitnessFunction;

import java.util.Random;

@SuppressWarnings("WeakerAccess")
public class Actor {
    private byte            gene;
    private float           mutationChance;
    private FitnessFunction fitness;

    public Actor(float mutationChance, FitnessFunction fitness) {
        this.mutationChance = mutationChance;
        this.fitness = fitness;

        byte[] bytes = new byte[1];
        new Random().nextBytes(bytes);
        this.gene = bytes[0];
    }

    public Actor(byte gene, FitnessFunction fitness, float mutationChance) {
        this.gene = gene;
        this.fitness = fitness;
        this.mutationChance = mutationChance;
    }

    public void doMutate() {
        for (int i = 0; i < 8; i++) {
            if (mutate()) {
                this.gene ^= 1 << (7 - i);
            }
        }
    }

    private boolean mutate() {
        return Math.random() <= mutationChance;
    }

    public float getMutationChance() {
        return mutationChance;
    }

    public byte getRawGene() {
        return gene;
    }

    public int getGeneValue() {
        return gene & 0xFF;
    }

    private String byteToBinaryString(byte bait) {
        return Integer.toBinaryString((bait & 0xFF) + 0x100).substring(1);
    }

    public FitnessFunction getFitness() {
        return fitness;
    }

    @Override
    public String toString() {
        return "Actor{" +
                "gene=" + byteToBinaryString(gene) +
                ", value=" + getGeneValue() +
                ", fitness=" + fitness +
                '}';
    }
}
