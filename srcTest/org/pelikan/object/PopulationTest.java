package org.pelikan.object;

import org.junit.Test;

import static org.junit.Assert.*;

public class PopulationTest {
    static Population population = new Population(2, null, 0.1f, 0.9f);

    @org.junit.Test
    public void doCrossover() {

        System.out.println(population);

        population.doCrossover();

        System.out.println(population);
    }

    @Test
    public void doMutate() {
        population.doMutate();
    }
}